package de.nurmarvin.modules.events;

import com.google.common.collect.Maps;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import java.util.HashMap;
import java.util.UUID;

public final class EventsModule {

    private HashMap<UUID, Location> lastLocations;

    public static void initialize(Plugin plugin) {
        new EventsModule(plugin);
    }

    private EventsModule(Plugin plugin) {
        this.lastLocations = Maps.newHashMap();

        plugin.getServer().getScheduler().runTaskTimerAsynchronously(plugin, () ->
        {
            for(Player player : plugin.getServer().getOnlinePlayers())
            {
                if(!this.lastLocations.containsKey(player.getUniqueId()))
                    this.lastLocations.put(player.getUniqueId(), player.getLocation());

                Location location = this.lastLocations.get(player.getUniqueId());
                if(this.lastLocations.containsKey(player.getUniqueId())) {
                    if (player.getLocation().getX() == location.getX()
                        && player.getLocation().getY() ==location.getY()
                        && player.getLocation().getZ() == location.getZ())
                        continue;
                }
                AsyncPlayerMoveEvent
                        event = new AsyncPlayerMoveEvent(player, location, player.getLocation());
                plugin.getServer().getPluginManager().callEvent(event);

                if(event.isCancelled())
                    if(event.getFrom() != null)
                        event.getPlayer().teleport(event.getFrom());
                this.lastLocations.put(player.getUniqueId(), player.getLocation());
            }
        }, 0, 1);
    }
}
