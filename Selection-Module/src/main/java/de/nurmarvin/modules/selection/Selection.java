package de.nurmarvin.modules.selection;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.bukkit.Location;

@Data @NoArgsConstructor
public final class Selection
{
    private Location pos1;
    private Location pos2;

    public enum SelectionType {
        FIRST,
        SECOND;
    }
}
