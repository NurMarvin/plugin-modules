package de.nurmarvin.modules.selection;

import com.google.common.collect.Maps;
import de.nurmarvin.modules.utils.Cuboid;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;

import java.util.HashMap;
import java.util.UUID;

public final class SelectionManager {
    @Getter
    private static SelectionManager instance;
    private HashMap<UUID, Selection> selections;
    @Getter @Setter
    private ItemStack selectionItem;
    @Getter @Setter
    private String selectionPermission;

    public SelectionManager(Plugin plugin) {
        instance = this;
        this.selections = Maps.newHashMap();
        plugin.getServer().getPluginManager().registerEvents(new PlayerInteractListener(), plugin);
    }

    void makeSelection(Player player, Selection.SelectionType type, Location location) {
        Selection selection = this.getSelection(player);

        if(type == Selection.SelectionType.FIRST) selection.setPos1(location);
        else selection.setPos2(location);

        if(selection.getPos1() != null && selection.getPos2() != null
           && selection.getPos1().getWorld() == selection.getPos2().getWorld()) {
            Cuboid cuboid = new Cuboid(selection.getPos1(), selection.getPos2());

            cuboid.sendWallsAsFakeBlocks(player);
        }
    }

    public Selection getSelection(Player player)
    {
        Selection selection = this.selections.get(player.getUniqueId());

        if(selection == null) selection = this.selections.put(player.getUniqueId(), new Selection());

        return selection;
    }
}
