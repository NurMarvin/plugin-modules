package de.nurmarvin.modules.selection;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

public final class PlayerInteractListener implements Listener {
    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent event)
    {
        Player player = event.getPlayer();

        if(event.getAction() != Action.LEFT_CLICK_BLOCK && event.getAction() != Action.RIGHT_CLICK_BLOCK)
            return;

        if(event.getItem() == SelectionManager.getInstance().getSelectionItem()
           && player.hasPermission(SelectionManager.getInstance().getSelectionPermission())) {
            SelectionManager.getInstance().makeSelection(player,
                                                         event.getAction() == Action.LEFT_CLICK_BLOCK ?
                                                         Selection.SelectionType.FIRST :
                                                         Selection.SelectionType.SECOND,
                                                         event.getClickedBlock().getLocation());
        }
    }
}
