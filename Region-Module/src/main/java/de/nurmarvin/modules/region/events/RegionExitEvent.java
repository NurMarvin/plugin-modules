package de.nurmarvin.modules.region.events;

import de.nurmarvin.modules.region.Region;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerEvent;

@Data
@EqualsAndHashCode(callSuper = false)
public class RegionExitEvent extends PlayerEvent implements Cancellable {
    private static final HandlerList HANDLER_LIST = new HandlerList();
    private Region region;
    private boolean cancelled = false;

    public RegionExitEvent(Player who, Region region) {
        super(who);
        this.region = region;
    }

    public static HandlerList getHandlerList() {
        return HANDLER_LIST;
    }

    @Override
    public HandlerList getHandlers() {
        return HANDLER_LIST;
    }
}
