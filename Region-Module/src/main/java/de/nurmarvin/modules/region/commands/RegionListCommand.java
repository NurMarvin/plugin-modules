package de.nurmarvin.modules.region.commands;

import de.nurmarvin.command.Command;
import de.nurmarvin.modules.region.Region;
import de.nurmarvin.modules.region.RegionManager;
import org.bukkit.command.CommandSender;

public class RegionListCommand extends Command {
    RegionListCommand() {
        super("list");
    }

    @Override
    public void process(CommandSender commandSender, String[] args) {
        StringBuilder stringBuilder = new StringBuilder();

        int i = 0;
        for (Region region : RegionManager.getInstance().getRegions().values()) {
            if (i != 0)
                stringBuilder.append(", ");
            stringBuilder.append(region.getName());
            i++;
        }

        commandSender.sendMessage("§eRegions: " + stringBuilder.toString());
    }
}
