package de.nurmarvin.modules.region.commands;

import de.nurmarvin.command.Command;

public class RegionCommand extends Command {
    public RegionCommand() {
        super("region");
        this.addAlias("rg");
        this.addSubCommand(new RegionSetCommand());
        this.addSubCommand(new RegionListCommand());
        this.addSubCommand(new RegionRemoveCommand());
        this.addSubCommand(new RegionTeleportCommand());
    }
}
