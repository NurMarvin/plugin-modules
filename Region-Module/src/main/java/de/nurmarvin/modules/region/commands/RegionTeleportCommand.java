package de.nurmarvin.modules.region.commands;

import de.nurmarvin.command.Command;
import de.nurmarvin.modules.region.RegionManager;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.util.StringUtil;

import java.util.ArrayList;
import java.util.List;

public class RegionTeleportCommand extends Command {
    RegionTeleportCommand() {
        super("teleport");
        this.addAlias("tp");
    }

    @Override
    public void process(CommandSender commandSender, String[] args) {
        if (this.isPlayer(commandSender, true)) {
            Player player = (Player) commandSender;
            if (RegionManager.getInstance().getRegions().containsKey(args[1].toLowerCase())) {
                player.sendMessage("§eYou were teleported to the region §a" + args[1] + "§e!");
                player.teleport(
                        RegionManager.getInstance().getRegions().get(args[1].toLowerCase()).getCuboid().getUpperSW());
            } else {
                player.sendMessage("§cThis region doesn't exist!");
            }
        }
    }

    @Override
    public List<String> suggest(CommandSender commandSender, String[] args) {
        if(args.length < 2) return StringUtil
                .copyPartialMatches(args[0], RegionManager.getInstance().getRegions().keySet(),
                                    new ArrayList<>(RegionManager.getInstance().getRegions().size()));
        return super.suggest(commandSender, args);
    }
}
