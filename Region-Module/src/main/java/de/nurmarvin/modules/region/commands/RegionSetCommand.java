package de.nurmarvin.modules.region.commands;

import de.nurmarvin.command.Command;
import de.nurmarvin.modules.region.Region;
import de.nurmarvin.modules.region.RegionManager;
import de.nurmarvin.modules.selection.Selection;
import de.nurmarvin.modules.selection.SelectionManager;
import de.nurmarvin.modules.utils.Cuboid;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class RegionSetCommand extends Command {
    RegionSetCommand() {
        super("set");
    }

    @Override
    public void process(CommandSender commandSender, String[] args) {
        if (this.isPlayer(commandSender, true)) {
            Player player = (Player) commandSender;
            Selection selection = SelectionManager.getInstance().getSelection(player);
            if (selection.getPos1() == null || selection.getPos2() == null) {
                player.sendMessage("§cYou haven't made a selection yet!");
                return;
            }

            RegionManager.getInstance().getRegions().put(args[1].toLowerCase(),
                                                         new Region(args[1], new Cuboid(selection.getPos1(),
                                                                                        selection.getPos2())));
            player.sendMessage("§eThe region §a" + args[1] + "§e was successfully created!");
        }
    }
}
