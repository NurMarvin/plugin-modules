package de.nurmarvin.modules.region;

import de.nurmarvin.modules.events.AsyncPlayerMoveEvent;
import de.nurmarvin.modules.region.events.RegionEnterEvent;
import de.nurmarvin.modules.region.events.RegionExitEvent;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import java.util.ArrayList;

public class PlayerMoveListener implements Listener {
    @EventHandler
    public void onPlayerMove(AsyncPlayerMoveEvent event) {
        Player player = event.getPlayer();
        RegionManager regionManager = RegionManager.getInstance();

        if (regionManager.getPlayerInRegions().containsKey(player.getUniqueId())) {
            ArrayList<Region> regions = new ArrayList<>(regionManager.getPlayerInRegions().get(player.getUniqueId()));
            for (Region region : regions) {
                if (!region.getCuboid().contains(player.getLocation())) {
                    RegionExitEvent exitEvent =
                            new RegionExitEvent(player, region);
                    Bukkit.getServer().getPluginManager().callEvent(exitEvent);
                    event.setCancelled(exitEvent.isCancelled());

                    if (!event.isCancelled())
                        regionManager.exitRegion(player, region);
                }
            }
        }
        for (Region region : regionManager.getRegions().values()) {
            if (regionManager.getPlayerInRegions().containsKey(player.getUniqueId())
                && regionManager.getPlayerInRegions().get(player.getUniqueId()).contains(region))
                return;
            if (region.getCuboid().contains(player.getLocation())) {
                RegionEnterEvent enterEvent = new RegionEnterEvent(player, region);
                Bukkit.getServer().getPluginManager().callEvent(enterEvent);
                event.setCancelled(enterEvent.isCancelled());

                if (!event.isCancelled())
                    regionManager.enterRegion(player, region);
            }
        }
    }
}