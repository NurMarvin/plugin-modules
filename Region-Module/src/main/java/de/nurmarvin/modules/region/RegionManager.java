package de.nurmarvin.modules.region;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import de.nurmarvin.command.CommandRegistry;
import de.nurmarvin.modules.region.commands.RegionCommand;
import de.nurmarvin.modules.utils.Cuboid;
import lombok.Data;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Level;

@Data
public class RegionManager {
    @Getter
    private static RegionManager instance;
    private final HashMap<String, Region> regions;
    private final File regionsFile;
    private final Gson gson;
    private HashMap<UUID, ArrayList<Region>> playerInRegions;

    public RegionManager(Plugin plugin) {
        instance = this;
        this.regions = Maps.newHashMap();
        this.gson = new Gson();
        this.playerInRegions = Maps.newHashMap();
        this.regionsFile = new File(plugin.getDataFolder(), "regions.json");
        this.loadRegions();

        CommandRegistry.getInstance().registerCommand(new RegionCommand());
    }

    private void loadRegions() {
        Bukkit.getServer().getLogger().info("--- Loading regions... ---");
        if (!regionsFile.exists()) {
            try {
                if (regionsFile.createNewFile())
                    Bukkit.getLogger().log(Level.INFO, "Regions file created.");
            } catch (IOException e) {
                Bukkit.getLogger().log(Level.SEVERE, "Failed to create regions file", e);
            }
        }

        try {
            JsonObject jsonObject = gson.fromJson(new FileReader(regionsFile), JsonObject.class);

            if (jsonObject == null)
                return;

            if (!jsonObject.has("regions") || !jsonObject.get("regions").isJsonObject())
                return;

            JsonObject regions = jsonObject.getAsJsonObject("regions");

            for (Map.Entry<String, JsonElement> region : regions.entrySet()) {
                if (!region.getValue().isJsonObject()) {
                    throw new JsonParseException("Expected JsonObject but got something different");
                }

                JsonObject pos1 = region.getValue().getAsJsonObject().get("pos1").getAsJsonObject();
                JsonObject pos2 = region.getValue().getAsJsonObject().get("pos2").getAsJsonObject();

                Location location1 = new Location(Bukkit.getServer().getWorld(pos1.get("world").getAsString()),
                                                  pos1.get("x").getAsDouble(), pos1.get("y").getAsDouble(),
                                                  pos1.get("z").getAsDouble());
                Location location2 = new Location(Bukkit.getServer().getWorld(pos2.get("world").getAsString()),
                                                  pos2.get("x").getAsDouble(), pos2.get("y").getAsDouble(),
                                                  pos2.get("z").getAsDouble());

                Region theRegion = new Region(region.getKey(), new Cuboid(location1, location2));

                this.regions.put(region.getKey().toLowerCase(), theRegion);

                Bukkit.getServer().getLogger().info("Loaded region: " + theRegion.toString());
            }
            Bukkit.getServer().getLogger().info("--- Regions loaded. ---");
        } catch (FileNotFoundException e) {
            Bukkit.getLogger().log(Level.SEVERE, "Failed to load regions file", e);
        }
    }

    public boolean isInRegion(Player player, String region) {
        if (this.getPlayerInRegions()
                .containsKey(player.getUniqueId()))
            for (Region regions : playerInRegions.get(player.getUniqueId()))
                if (regions.getName().equalsIgnoreCase(region))
                    return true;
        return false;
    }

    public boolean isInRegionType(Player player, String region) {
        if (this.getPlayerInRegions().containsKey(player.getUniqueId()))
            for (Region regions : playerInRegions.get(player.getUniqueId()))
                if (regions.getName().startsWith(region))
                    return true;
        return false;
    }

    public void saveRegions() {
        Bukkit.getServer().getLogger().info("--- Saving regions... ---");
        if (!regionsFile.exists()) {
            try {
                if (regionsFile.createNewFile())
                    Bukkit.getLogger().log(Level.INFO, "Regions file created");
            } catch (IOException e) {
                Bukkit.getLogger().log(Level.SEVERE, "Failed to create regions file", e);
            }
        }

        JsonObject jsonObject = new JsonObject();

        JsonObject regions = new JsonObject();

        for (Region region : this.regions.values()) {
            JsonObject regionObject = new JsonObject();

            JsonObject pos1 = new JsonObject();

            pos1.addProperty("world", region.getCuboid().getLocation1().getWorld().getName());
            pos1.addProperty("x", region.getCuboid().getLocation1().getX());
            pos1.addProperty("y", region.getCuboid().getLocation1().getY());
            pos1.addProperty("z", region.getCuboid().getLocation1().getZ());

            JsonObject pos2 = new JsonObject();

            pos2.addProperty("world", region.getCuboid().getLocation2().getWorld().getName());
            pos2.addProperty("x", region.getCuboid().getLocation2().getX());
            pos2.addProperty("y", region.getCuboid().getLocation2().getY());
            pos2.addProperty("z", region.getCuboid().getLocation2().getZ());

            regionObject.add("pos1", pos1);
            regionObject.add("pos2", pos2);

            regions.add(region.getName(), regionObject);
        }

        jsonObject.add("regions", regions);

        Writer writer;
        try {
            writer = new BufferedWriter(
                    new OutputStreamWriter(new FileOutputStream(regionsFile), StandardCharsets.UTF_8));
            writer.write(gson.toJson(jsonObject));
            writer.close();
        } catch (IOException e) {
            Bukkit.getLogger().log(Level.SEVERE, "Failed to save regions file", e);
        }

        Bukkit.getServer().getLogger().info("--- Regions saved. ---");
    }

    public Region getRegion(String name) {
        return this.regions.get(name.toLowerCase());
    }

    public void enterRegion(Player player, Region region) {
        if (this.playerInRegions.containsKey(player.getUniqueId())) {
            ArrayList<Region> regions = this.playerInRegions.get(player.getUniqueId());
            regions.add(region);
            this.playerInRegions.put(player.getUniqueId(), regions);
        } else {
            this.playerInRegions.put(player.getUniqueId(), Lists.newArrayList(region));
        }
    }

    public void exitRegion(Player player, Region region) {
        if (this.playerInRegions.containsKey(player.getUniqueId())) {
            ArrayList<Region> regions = this.playerInRegions.get(player.getUniqueId());
            for (int i = 0; i < regions.size(); i++) {
                Region aRegion = regions.get(i);
                if (aRegion.getName().equalsIgnoreCase(region.getName())) {
                    if (this.playerInRegions.size() == 1) {
                        this.playerInRegions.remove(player.getUniqueId());
                    } else {
                        regions.remove(aRegion);
                    }
                    return;
                }
            }
        }
    }
}
