package de.nurmarvin.modules.region;

import de.nurmarvin.modules.utils.Cuboid;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Region {
    private String name;
    private Cuboid cuboid;
}
