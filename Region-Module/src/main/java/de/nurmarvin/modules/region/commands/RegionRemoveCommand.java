package de.nurmarvin.modules.region.commands;

import de.nurmarvin.command.Command;
import de.nurmarvin.modules.region.RegionManager;
import org.bukkit.command.CommandSender;
import org.bukkit.util.StringUtil;

import java.util.ArrayList;
import java.util.List;

public class RegionRemoveCommand extends Command {
    RegionRemoveCommand() {
        super("remove");
    }

    @Override
    public void process(CommandSender commandSender, String[] args) {
        if (RegionManager.getInstance().getRegions().containsKey(args[1].toLowerCase())) {
            commandSender.sendMessage("§eThe region §a" + args[1] + "§e was successfully removed!");
            RegionManager.getInstance().getRegions().remove(args[1].toLowerCase());
        } else {
            commandSender.sendMessage("§cThis region doesn't exist!");
        }
    }

    @Override
    public List<String> suggest(CommandSender commandSender, String[] args) {
        if(args.length < 2) return StringUtil
                .copyPartialMatches(args[0], RegionManager.getInstance().getRegions().keySet(),
                                    new ArrayList<>(RegionManager.getInstance().getRegions().size()));
        return super.suggest(commandSender, args);
    }
}
