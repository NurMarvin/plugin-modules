package de.nurmarvin.command;

import com.google.common.collect.Lists;
import de.nurmarvin.command.condition.Condition;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.util.StringUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public abstract class Command
        implements CommandExecutable {
    private List<String> knownNames = Lists.newArrayList();
    private List<Condition> conditions = Lists.newArrayList();
    private List<Command> subCommands = Lists.newArrayList();

    public Command(String name) {
        addAlias(name);
    }

    public void addAlias(String command) {
        String lowercase = command.toLowerCase();
        if (this.knownNames.contains(lowercase)) {
            throw new IllegalArgumentException("Alias can not duplicate");
        }
        this.knownNames.add(lowercase);
    }

    public List<String> getAliases() {
        return this.knownNames;
    }

    public String getName() {
        return this.knownNames.get(0);
    }

    public void addCondition(Condition condition, String... args) {
        if (args.length == 0) {
            this.conditions.add(condition);
            return;
        }
        String lowercase = args[0].toLowerCase();

        Command sub = null;
        for (Command commands : this.subCommands) {
            if (commands.knownNames.contains(lowercase)) {
                sub = commands;
            }
        }
        if (sub != null) {
            String[] newArgs = Arrays.copyOfRange(args, 1, args.length);
            sub.addCondition(condition, newArgs);
            return;
        }
        this.conditions.add(condition);
    }

    public void addSubCommand(Command command) {
        this.subCommands.add(command);
    }

    public void execute(CommandSender sender, String[] args) {
        for (Condition condition : this.conditions) {
            if (!condition.canProcess(sender, args)) {
                return;
            }
        }
        if (args.length == 0) {
            if (args.length >= minArgs()) {
                process(sender, args);
            } else {
                sender.sendMessage(describeUsage());
            }
            return;
        }
        String lowercase = args[0].toLowerCase();

        Command sub = null;
        for (Command commands : this.subCommands) {
            if (commands.knownNames.contains(lowercase)) {
                sub = commands;
            }
        }
        if (sub != null) {
            String[] newArgs = Arrays.copyOfRange(args, 1, args.length);
            sub.execute(sender, newArgs);
            return;
        }
        if (args.length >= minArgs()) {
            process(sender, args);
        } else {
            sender.sendMessage(describeUsage());
        }
    }

    public void process(CommandSender commandSender, String[] args) {
        if (args.length == 0) {
            commandSender.sendMessage(describeUsage());
            commandSender.sendMessage(ChatColor.RED + describeSubCommands());
        } else {
            commandSender.sendMessage(ChatColor.RED + "Unknown Subcommand");
            commandSender.sendMessage(ChatColor.RED + describeSubCommands());
        }
    }

    private String describeSubCommands() {
        if (this.subCommands.isEmpty()) {
            return null;
        }
        StringBuilder builder = new StringBuilder();
        builder.append("/");
        builder.append(this.knownNames.get(0));
        builder.append(" <");
        for (Command commands : this.subCommands) {
            builder.append(commands.knownNames.get(0));
            builder.append(", ");
        }
        builder.delete(builder.length() - 2, builder.length());
        builder.append(">");
        return builder.toString();
    }

    public int minArgs() {
        return 0;
    }

    public List<String> suggest(CommandSender commandSender, String[] args) {
        if (this.subCommands.isEmpty()) {
            return Collections.emptyList();
        }

        String lowercase = args[0].toLowerCase();

        Command sub = null;
        for (Command commands : this.subCommands) {
            if (commands.knownNames.contains(lowercase)) {
                sub = commands;
            }
        }
        if (sub != null) {
            String[] newArgs = Arrays.copyOfRange(args, 1, args.length);
            return sub.suggest(commandSender, newArgs);
        }

        ArrayList<String> list = Lists.newArrayList();
        for (Command commands : this.subCommands) {
            list.add(commands.knownNames.get(0));
        }

        return list;
    }

    protected boolean playerProvided(Player sender, int arg, String[] args) {
        if (args.length < arg) {
            sender.sendMessage(describeUsage());
            return false;
        }

        return Bukkit.getPlayerExact(args[arg]) != null;
    }

    protected List<String> getPlayerSuggestions(String arg) {
        return StringUtil.copyPartialMatches(arg, Bukkit.getOnlinePlayers().stream().map(HumanEntity::getName)
                                                        .collect(Collectors.toList()), Lists.newArrayList());
    }

    public boolean isPlayer(CommandSender sender, boolean inform)
    {
        if(sender instanceof Player) return true;
        if(inform) sender.sendMessage("§cYou must be a player to run this command!");
        return false;
    }

    public String describeUsage() {
        return ChatColor.RED + "Too few arguments!";
    }
}
