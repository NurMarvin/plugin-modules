package de.nurmarvin.command.condition;

import org.bukkit.command.CommandSender;

public interface Condition {
    boolean canProcess(CommandSender sender, String[] args);
}
