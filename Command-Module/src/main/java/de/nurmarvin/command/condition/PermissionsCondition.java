package de.nurmarvin.command.condition;

import org.bukkit.command.CommandSender;

import java.beans.ConstructorProperties;

public class PermissionsCondition
        implements Condition {
    private final String permissions;

    @ConstructorProperties({"rank"})
    public PermissionsCondition(String permissions) {
        this.permissions = permissions;
    }

    public boolean canProcess(CommandSender sender, String[] args) {
        return sender.hasPermission(this.permissions);
    }
}
