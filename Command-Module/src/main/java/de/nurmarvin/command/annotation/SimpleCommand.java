package de.nurmarvin.command.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface SimpleCommand {
    String permissions() default "";

    String name();

    String[] aliases() default {};
}
