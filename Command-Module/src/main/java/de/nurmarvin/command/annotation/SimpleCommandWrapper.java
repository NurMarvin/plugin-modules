package de.nurmarvin.command.annotation;

import com.google.common.collect.Lists;
import de.nurmarvin.command.CommandExecutable;
import de.nurmarvin.command.condition.Condition;
import org.bukkit.command.CommandSender;

import java.lang.reflect.Method;
import java.util.List;

public class SimpleCommandWrapper
        implements CommandExecutable {
    private Object object;
    private Method method;
    private String permissions;
    private List<Condition> conditions;

    public SimpleCommandWrapper(Object object, Method method, String permissions) {
        this.conditions = Lists.newArrayList();
        this.object = object;
        (this.method = method).setAccessible(true);
        this.permissions = permissions;
    }

    public void addCondition(Condition condition) {
        this.conditions.add(condition);
    }

    public void execute(CommandSender sender, String[] args) {
        try {
            if (!sender.hasPermission(this.permissions))
                return;
            for (Condition condition : this.conditions) {
                if (!condition.canProcess(sender, args)) {
                    return;
                }
            }
            this.method.invoke(this.object, sender, args);
        } catch (Throwable ignored) {
        }
    }
}
