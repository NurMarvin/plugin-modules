package de.nurmarvin.command;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Maps;
import de.nurmarvin.command.annotation.SimpleCommand;
import de.nurmarvin.command.annotation.SimpleCommandWrapper;
import de.nurmarvin.command.condition.Condition;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandMap;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class CommandRegistry {

    @Getter
    private static CommandRegistry instance = new CommandRegistry();
    private CommandMap commandMap = getCommandMap();
    @Getter
    private Map<String, CommandExecutable> commands = Maps.newHashMap();
    private CommandWrapper commandWrapper = new CommandWrapper();

    public void provideCondition(String command, Condition condition) {
        if (command.contains(" ")) {
            String[] split = command.split(" ");
            Preconditions.checkArgument(this.commands.containsKey(split[0]),
                                        "Could not find command " + split[0] + "whilst providing condition " +
                                        condition.getClass().getName() + "available commands: " +
                                        this.commands.keySet());
            CommandExecutable commandObj = this.commands.get(split[0]);
            Preconditions.checkState(commandObj instanceof Command,
                                     "The registered command type could not provide condition");
            ((Command) commandObj).addCondition(condition, (String[]) Arrays.copyOfRange(split, 1, split.length));
        } else {
            Preconditions.checkArgument(this.commands.containsKey(command),
                                        "Could not find command " + command + " whilst providing condition for it");
            CommandExecutable commandObj = this.commands.get(command);
            if ((commandObj instanceof Command)) {
                ((Command) commandObj).addCondition(condition);
            } else if ((commandObj instanceof SimpleCommandWrapper)) {
                ((SimpleCommandWrapper) commandObj).addCondition(condition);
            } else Preconditions.checkState(false, "The registered command type can't have conditions.");
        }
    }

    public CommandMap getCommandMap() {
        try {
            Field commandMap = Bukkit.getServer().getClass().getDeclaredField("commandMap");
            commandMap.setAccessible(true);
            return (CommandMap) commandMap.get(Bukkit.getServer());
        } catch (Throwable ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public void registerCommand(Command object) {
        registerCommand(object, null);
    }

    public void registerCommand(Command object, String provider) {
        Plugin plugin = JavaPlugin.getProvidingPlugin(object.getClass());
        Preconditions.checkNotNull(plugin, "Class must be loaded from JavaPlugin");
        if ((provider == null) || (provider.isEmpty())) {
            provider = plugin.getName();
        }
        this.commands.put(object.getName(), object);
        CustomCommand pluginCommand = newCustomCommand(object.getName(), plugin, object.getAliases().
                toArray(new String[0]));
        this.commandMap.register(provider, pluginCommand);
    }

    public void registerSimpleCommand(Object object) {
        registerSimpleCommand(object, null);
    }

    public void registerSimpleCommand(Object object, String provider) {
        Plugin plugin = JavaPlugin.getProvidingPlugin(object.getClass());
        Preconditions.checkNotNull(plugin, "Class must be loaded from JavaPlugin");
        if ((provider == null) || (provider.isEmpty())) {
            provider = plugin.getName();
        }
        for (Method method : object.getClass().getDeclaredMethods()) {
            if (method.isAnnotationPresent(SimpleCommand.class)) {
                SimpleCommand simple = method.getAnnotation(SimpleCommand.class);
                SimpleCommandWrapper wrap = new SimpleCommandWrapper(object, method, simple.permissions());
                this.commands.put(simple.name(), wrap);
                CustomCommand pluginCommand = newCustomCommand(simple.name(), plugin, simple.aliases());
                this.commandMap.register(provider, pluginCommand);
            }
        }
    }

    private CustomCommand newCustomCommand(String name, Plugin plugin, String[] alias) {
        CustomCommand pluginCommand = new CustomCommand(name, plugin);
        if (alias.length != 0) {
            pluginCommand.setAliases(Arrays.asList(alias));
        }
        pluginCommand.setExecutor(this.commandWrapper);
        return pluginCommand;
    }

    public class CommandWrapper
            implements TabCompleter, CommandExecutor {
        public CommandWrapper() {
        }

        @Override
        public boolean onCommand(CommandSender sender, org.bukkit.command.Command command, String label,
                                 String[] args) {

            (CommandRegistry.this.commands.get(command.getName())).execute(sender, args);
            return true;
        }

        public List<String> onTabComplete(CommandSender sender, org.bukkit.command.Command command, String label,
                                          String[] args) {
            CommandExecutable executable = CommandRegistry.this.commands.get(command.getName());
            if ((executable instanceof Command)) {
                return ((Command) executable).suggest(sender, args);
            }
            return ImmutableList.of();
        }
    }
}
